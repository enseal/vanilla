def update_custom_script(doc, code):
    script = doc.get('script') or ''
    new_script = ''

    if script.strip() and code not in script:
        new_script = '{0}\n{1}'.format(script, code).strip()
    elif not script.strip():
        new_script = code

    if new_script:
        doc.script = new_script
        doc.save()
